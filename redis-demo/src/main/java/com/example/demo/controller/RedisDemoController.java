package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.service.UserService;
import com.example.demo.utils.RedisUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * @ClassName RedisDemoController
 * @Description TODO
 * @Author lyk
 * @Date 2019/3/11 10:25
 * @Version 1.0
 **/
@RestController
@RequestMapping("/demo")
public class RedisDemoController {

    @Autowired
    private UserService userService;

    @Autowired
    private RedisUtils redisUtils;

    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    @ApiOperation(value = "addUser", tags = "添加用户", notes = "添加用户")
    public void addUser(@RequestBody User user) {
//        User user = new User();
//        user.setId(1);
//        user.setName("tom");
//        user.setAddress("xia'an");
//        user.setAge(20);
//        user.setPhone("15136231026");

        userService.addUser(user);
    }

    @RequestMapping(value = "/getUser", method = RequestMethod.POST)
    @ApiOperation(value = "getUser", tags = "获取用户", notes = "获取用户")
    public String getUser(@RequestBody User user) {

        return userService.getUser(user.getName());
    }

    @RequestMapping(value = "/getUserList", method = RequestMethod.POST)
    @ApiOperation(value = "getUserList", tags = "获取用户列表", notes = "获取用户列表")
    public List<User> getUserList(@RequestBody User user) {

        return userService.getUserList(user.getName());
    }

    @RequestMapping("/add")
    public void add() {
        List<Object> list = new ArrayList<>();
        list.add("test");
        redisUtils.setList("test", list);
    }

    @RequestMapping("/addSet")
    public void addSet() {
        Set<String> set = new HashSet<>();
        set.add("ff0f8335b26516e229afcd5c1950e374");
        set.add("7bc8b15811ec1aa35e3c9cdf75d6fd41");
        set.add("30cd88076845572613265607a4d33755");
        set.add("5aea41150f2eea96c65986ac85b34f21");
        set.add("6d023732103095e67719a6bc340f9ed1");

        redisUtils.setSet("stdId", set);

        Set<User> userSet = new HashSet<>();
        User user1 = new User();
        user1.setId(1);
        user1.setAddress("xi'an");
        user1.setName("AAA");
        user1.setEmail("123@123.com");
        user1.setPhone("11111111111");
        user1.setAge(20);
        user1.setPassword("123456");

        User user2 = new User();
        user2.setId(2);
        user2.setAddress("xianyang");
        user2.setName("BBB");
        user2.setEmail("123@123.com");
        user2.setPhone("22222222222");
        user2.setAge(30);
        user2.setPassword("123456");

        userSet.add(user1);
        userSet.add(user2);

        redisUtils.setSet("user",userSet);

    }

}

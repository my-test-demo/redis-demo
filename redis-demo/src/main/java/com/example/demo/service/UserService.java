package com.example.demo.service;

import com.example.demo.entity.User;

import java.util.List;

/**
 * @ClassName UserService
 * @Description TODO
 * @Author lyk
 * @Date 2019/3/11 10:34
 * @Version 1.0
 **/
public interface UserService {

    void addUser(User user);

    String getUser(String name);

    List<User> getUserList(String name);
}

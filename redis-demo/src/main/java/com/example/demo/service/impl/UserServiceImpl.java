package com.example.demo.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.dao.UserMapper;
import com.example.demo.entity.User;
import com.example.demo.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName RedisDemoServiceImpl
 * @Description TODO
 * @Author lyk
 * @Date 2019/3/11 10:35
 * @Version 1.0
 **/
@Slf4j
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Value("${demo.redis.userRedisFormat}")
    private String userRedisFormat;

    @Value("${demo.redis.timeOut}")
    private Long timeOut;

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private UserMapper userMapper;


    @Override
    public void addUser(User user) {

//        userMapper.addUser(user);

        JSONObject userJson = (JSONObject) JSONObject.toJSON(user);

        redisTemplate.opsForValue().set(String.format(userRedisFormat, user.getName()), userJson.toJSONString(), timeOut, TimeUnit.SECONDS);

    }

    @Override
    public String getUser(String name) {

        Object user = redisTemplate.opsForValue().get(String.format(userRedisFormat, name));

        return user.toString();

    }

    @Override
    public List<User> getUserList(String name) {
        return userMapper.getUser(name);
    }
}

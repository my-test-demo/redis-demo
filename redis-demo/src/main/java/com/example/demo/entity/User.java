package com.example.demo.entity;

import lombok.Data;

/**
 * @ClassName User
 * @Description TODO
 * @Author lyk
 * @Date 2019/3/11 10:21
 * @Version 1.0
 **/
@Data
public class User {

    private Integer id;
    /**
     * @Description:地址
     */
    private String address;
    /**
     * @Description: 用户姓名
     */
    private String name;
    /**
     * @Description: 用户邮箱
     */
    private String email;
    /**
     * @Description:电话
     */
    private String phone;
    /**
     * @Description: 用户年龄
     */
    private Integer age;
    /**
     * @Description: 用户密码
     */
    private String password;
}

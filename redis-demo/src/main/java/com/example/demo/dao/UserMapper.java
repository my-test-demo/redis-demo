package com.example.demo.dao;

import com.example.demo.entity.User;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 * @InterfaceName UserMapper
 * @Description TODO
 * @Author lyk
 * @Date 2019/3/11 14:53
 * @Version 1.0
 **/
//@Component
@Mapper
public interface UserMapper {
    void addUser(User user);

    List<User> getUser(String name);
}

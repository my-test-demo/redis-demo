package com.example.demo.test;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.*;

/**
 * @ClassName RedisTest
 * @Description TODO
 * @Author lyk
 * @Date 2019/3/11 17:15
 * @Version 1.0
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
//@WebAppConfiguration
@Slf4j
public class RedisTest {

    @Value("${demo.redis.userRedisFormat}")
    private String userRedisFormat;

    @Value("${demo.redis.stringRedisFormat}")
    private String stringRedisFormat;

    @Value("${demo.redis.listRedisFormat}")
    private String listRedisFormat;

    @Value("${demo.redis.setRedisFormat}")
    private String setRedisFormat;

    @Value("${demo.redis.mapRedisFormat}")
    private String mapRedisFormat;

    @Value("${demo.redis.timeOut}")
    private Long timeOut;

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Test
    public void addString() {
        redisTemplate.opsForValue().set(stringRedisFormat.concat("1"), "test01");

        String result1 = redisTemplate.opsForValue().get(stringRedisFormat.concat("1")).toString();

        System.out.println(stringRedisFormat.concat("1") + ":" + result1);
    }

    @Test
    public void leftPushList() {
       List<String> list = new ArrayList<String>();
        list.add("aa");
        list.add("bb ");
        for (int i = 0 ; i < 10 ;i++){
        redisTemplate.opsForList().leftPush("list", i);
        }

        //List<String> resultList = (List<String>) redisTemplate.opsForList().leftPop("list");
       // System.out.println(resultList);

    }
    @Test
    public void addList() {
        List<String> list = new ArrayList<String>();
        list.add("aa");
        list.add("bb");

        redisTemplate.opsForList().leftPush("list", list);

        List<String> resultList = (List<String>) redisTemplate.opsForList().leftPop("list");
        System.out.println(resultList);

    }

    @Test
    public void addMap() {
        Map<String, String> map = new HashMap();
        map.put("key1", "value1");
        map.put("key2", "value2");
//        redisTemplate.opsForValue().set(mapRedisFormat, map, timeOut, TimeUnit.SECONDS);

        redisTemplate.opsForHash().putAll(mapRedisFormat.concat("map1"), map);

        Map<Object, Object> resultMap = redisTemplate.opsForHash().entries(mapRedisFormat.concat("map1"));
        System.out.println("Map1:" + resultMap);
    }

    @Test
    public void addSet() {
        Set<String> set = new HashSet<String>();
        set.add("Test1");
        set.add("Test2");
        set.add("Test3");
        redisTemplate.opsForSet().add(setRedisFormat, set);

        Set<Object> resultSet = redisTemplate.opsForSet().members(setRedisFormat);
        System.out.println("set:" + resultSet);

    }

    @Test
    public void del(){
        redisTemplate.delete(stringRedisFormat.concat("1"));
    }


    @Test
    public void getList() {
        List<Object> list = redisTemplate.opsForList().range("redis_mq_addr_info_list2019_07_15_15", 0, 2);
        System.out.println(list);

    }
}

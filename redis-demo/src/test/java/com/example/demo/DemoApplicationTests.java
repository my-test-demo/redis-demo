package com.example.demo;

import com.alibaba.fastjson.JSON;
import com.example.demo.entity.User;
import com.example.demo.utils.RedisUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

    @Autowired
    RedisUtils redisUtils;

    @Test
    public void getSet() {
        Set<Object> set = redisUtils.getSet("user");
        System.out.println(set);
    }

    @Test
    public void hashSet() {
        Set<String> set = new HashSet<>();
        set.add("ff0f8335b26516e229afcd5c1950e374");
        set.add("7bc8b15811ec1aa35e3c9cdf75d6fd41");
        set.add("30cd88076845572613265607a4d33755");
        set.add("5aea41150f2eea96c65986ac85b34f21");
        set.add("6d023732103095e67719a6bc340f9ed1");
        redisUtils.setHash("stdId_hash","stdId", set);
        redisUtils.setHash("stdId_hash","stdId", "1213");
    }
    @Test
    public void getHashSet() {
        Object obj = redisUtils.getHash("stdId_hash","stdId");
        System.out.println(JSON.toJSONString(obj));
    }
    @Test
    public void addSet() {
        Set<String> set = new HashSet<>();
        set.add("ff0f8335b26516e229afcd5c1950e374");
        set.add("7bc8b15811ec1aa35e3c9cdf75d6fd41");
        set.add("30cd88076845572613265607a4d33755");
        set.add("5aea41150f2eea96c65986ac85b34f21");
        set.add("6d023732103095e67719a6bc340f9ed1");

        redisUtils.setSet("stdId",set);

        Set<User> userSet = new HashSet<>();
        User user1 = new User();
        user1.setId(1);
        user1.setAddress("xi'an");
        user1.setName("AAA");
        user1.setEmail("123@123.com");
        user1.setPhone("11111111111");
        user1.setAge(20);
        user1.setPassword("123456");

        User user2 = new User();
        user2.setId(2);
        user2.setAddress("xianyang");
        user2.setName("BBB");
        user2.setEmail("123@123.com");
        user2.setPhone("22222222222");
        user2.setAge(30);
        user2.setPassword("123456");

        userSet.add(user1);
        userSet.add(user2);

        redisUtils.setSet("user", userSet);
        redisUtils.setSet("user", userSet);
    }

    @Test
    public void addList() {
        List<User> userList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            User user = new User();
            user.setId(i);
            user.setAddress("addr" + i);
            user.setName("Aan" + i);
            user.setEmail("123@126.com");
            user.setPhone("1570031560" + i);
            user.setAge(0);
            user.setPassword("123456" + i);

//            userList.add(user);
            redisUtils.setList("userList",user);
//            redisUtils.setList("userList", JSON.toJSONString(user));
        }
    }

    @Test
    public void getList() {
         List<Object> list = redisUtils.getList("2019_08_14:10_redis_mq_addr_info_list", 0, 10);

        System.out.println(list);
    }

    @Test
    public void get() {
        System.out.println(redisUtils.getKeys("*"));
    }

    @Test
    public void addList1(){
        List<Object> list = new ArrayList<>();
        list.add("redis7001");
        list.add("redis7002");
        list.add("redis7003");
        list.add("redis7004");
        redisUtils.setList("test", list);
    }
}
